# Systemd --user units

## bluetooth-auto-poweroff
Turns off bluetooth after 5 min from boot if nothing's connected.

Enable this service with:
```
systemctl --user start bluetooth-auto-poweroff.service bluetooth-auto-poweroff.timer
```
