#!/usr/bin/env sh

LAYOUTS_DIR=$HOME/.config/i3/layouts

i3-msg append_layout $LAYOUTS_DIR/navigation-vlc-layout.json

killall telegram-desktop

google-chrome-stable &
telegram-desktop -- %u &
