#!/usr/bin/env sh

LAYOUTS_DIR=$HOME/.config/i3/layouts

i3-msg append_layout $LAYOUTS_DIR/spotify-layout.json

killall spotify
killall telegram-desktop

# Run spotify (needs https://github.com/dasJ/spotifywm)
LD_PRELOAD=/usr/lib/libcurl.so.3:$HOME/spotifywm/spotifywm.so /usr/bin/spotify &
telegram-desktop -- %u &
