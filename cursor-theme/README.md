## Cursor Theme dotfiles
- `index.theme` file must be placed in `~/.icons/default/index.theme`, if necessary put it also in  `/usr/share/icons/default/index.theme`
- `settings.ini` file must be placed in `.config/gtk-3.0/settings.ini`, if necessary put it also in `/usr/share/gtk-3.0/settings.ini`
