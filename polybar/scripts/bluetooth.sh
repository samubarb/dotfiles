#!/bin/bash
#
# bluetooth.sh

SHOW_IF_NOTHING_CONNECTED="yes" # "yes"
HIDE_NUMBER="no" # Always hide number of connected devices
HIDE_ZERO_VALUE="yes" # "yes" Only working if SHOW_IF_NOTHING_CONNECTED=="yes" and if HIDE_NUMBER=="no"

icon_enabled=""
# icon_enabled=""
icon_disabled=""
# Check if Bluetooth is blocked or unblocked
status=$(rfkill list bluetooth --output SOFT | sed '2q;d')


if [ $status == "unblocked" ]; then
    # Number of connected devices
    devices=$(bluetoothctl devices | sed 's/.*\(..:..:..:..:..:..\).*/\1/' | xargs -n1 bluetoothctl info | grep "Connected: yes" | wc -l) 

    if ([ $SHOW_IF_NOTHING_CONNECTED == "yes" ] || [ $devices != "0" ]); then
        if [ $devices == "0" ] && [ $HIDE_ZERO_VALUE == "yes" ] || [ $HIDE_NUMBER == "yes" ]; then
        		echo "$icon_enabled"
        else
        	echo "$icon_enabled $devices"
        fi
    fi
else
    echo "$icon_disabled"
fi